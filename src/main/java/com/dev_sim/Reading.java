package com.dev_sim;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

@AllArgsConstructor
@Getter
@Setter
public class Reading {
    private Long timestamp;
    private Integer deviceId;
    private Double measuredValue;
}
