package com.dev_sim;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.opencsv.CSVReader;
import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

@Component
@Slf4j
public class Simulator {
    private final Integer DEVICE_ID;
    private CSVReader CSV_READER;

    @Value("${readings.csv.file.path}")
    private String CSV_FILE_PATH;

    @Value("${simulator.rabbitmq.readings.exchange}")
    private String readingsExchange;
    @Value("${simulator.rabbitmq.readings.routing_key}")
    private String readingsRoutingKey;

    @Autowired
    private RabbitTemplate rabbitTemplate;

    public Simulator() {
        DEVICE_ID = Integer.parseInt(System.getenv("deviceId"));
        log.info("Device ID is {}", DEVICE_ID);
    }

    @PostConstruct
    public void initiateCSVReader() throws FileNotFoundException {
        CSV_READER = new CSVReader(new FileReader(CSV_FILE_PATH));
    }

    @Scheduled(fixedDelay = 60000)
    public void simulate() throws IOException {
        String[] nextRecord = CSV_READER.readNext();
        if (nextRecord != null) {
            double measurementValue = Double.parseDouble(nextRecord[0]);
            long timestampInSeconds = System.currentTimeMillis() / 1000L;

            publishNewReadingMessage(new Reading(timestampInSeconds, DEVICE_ID, measurementValue));
        }
    }

    private void publishNewReadingMessage(Reading reading) throws IOException {
        rabbitTemplate.convertAndSend(readingsExchange, readingsRoutingKey, convertObjectToJson(reading));
        log.info("Sent readings message for deviceId={}, timestamp={}, measuredValue={}", reading.getDeviceId(), reading.getTimestamp(), reading.getMeasuredValue());
    }

    private String convertObjectToJson(Object object) throws IOException {
        ObjectMapper objectMapper = new ObjectMapper();
        return objectMapper.writeValueAsString(object);
    }
}
